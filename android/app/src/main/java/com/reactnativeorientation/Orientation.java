package com.oneworld.modules;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.WindowManager;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.HashMap;
import java.util.Map;

public class Orientation extends ReactContextBaseJavaModule implements LifecycleEventListener {

    private static String ORIENTATION_LANDSCAPE = "LANDSCAPE";
    private static String ORIENTATION_PORTRAIT  = "PORTRAIT";
    private ReactApplicationContext mContext = null;
    OrientationEventListener mOrientationEventListener;
    private String previousOrientation = null;

    public Orientation(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;

        mOrientationEventListener = new OrientationEventListener(mContext) {
            @Override
            public void onOrientationChanged(int orientation) {

                String mode;

                if(orientation >= 85 || orientation >= 265) {
                   mode = ORIENTATION_LANDSCAPE;
                } else {
                   mode = ORIENTATION_PORTRAIT;
                }

                if(previousOrientation == null) {
                    previousOrientation = mode;
                } else if(!previousOrientation.equals(mode)) {
                    previousOrientation = mode;

                    WritableMap params = Arguments.createMap();
                    params.putString("orientation", mode);

                    if(mContext.hasActiveCatalystInstance()) {
                        Log.d("Orientation", "Send Event because orientation is now " + mode);
                        mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                                .emit("OrientationChanged", params);
                    }
                }
            }
        };

        if(mOrientationEventListener.canDetectOrientation()) {
            mOrientationEventListener.enable();
        }

        reactContext.addLifecycleEventListener(this);
    }

    @Override
    public String getName() {
        return "OrientationLocker";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(ORIENTATION_LANDSCAPE, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        constants.put(ORIENTATION_PORTRAIT, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        return constants;
    }

    @ReactMethod
    public void lock(int orientation) {
        try {
            getCurrentActivity().setRequestedOrientation(orientation);
        } catch (NullPointerException err) {
            // Hello darnkess my old friend ...
        }
    }

    @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {
        mOrientationEventListener.disable();
    }
}
