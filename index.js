'use strict';

import { NativeEventEmitter, NativeModules } from 'react-native';

const OrientationLocker = NativeModules.OrientationLocker;
const LANDSCAPE = OrientationLocker.LANDSCAPE;
const PORTRAIT = OrientationLocker.PORTRAIT;

const OrientationLockerEmitter = new NativeEventEmitter(OrientationLocker);

let instance;

class Orientation {
  constructor(orientationCallback) {
    if(!instance) {
      instance = this;
    }

    this._callbackOrientation = orientationCallback;
    this._subscription = null;

    this.initListener();

    return instance;
  }

  initListener() {
    this._subscription = OrientationLockerEmitter.addListener('OrientationChanged', this.onOrientationChanged.bind(this))
  }

  removeListener() {
    if(this._subscription) {
      this._subscription.remove();
    } else {
      console.warn('Careful you are calling removeListener on a null subscription. Did you forget to init Orientation class ?');
    }
  }

  lock(orientation) {
    OrientationLocker.lock(orientation)
  }

  onOrientationChanged({orientation}) {
    this._callbackOrientation(orientation);
  }
}

export {
  Orientation,
  LANDSCAPE,
  PORTRAIT
}
