//
//  Orientation.h
//  OneWorld
//
//  Created by admin on 19/05/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface Orientation : RCTEventEmitter <RCTBridgeModule>

@end
