//
//  Orientation.m
//  OneWorld
//
//  Created by admin on 19/05/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//
#import "Orientation.h"
#import <React/RCTLog.h>

@implementation Orientation

RCT_EXPORT_MODULE(OrientationLocker);

- (NSArray<NSString *> *)supportedEvents
{
  return @[@"OrientationChanged"];
}

- (instancetype)init
{
  if((self = [super init])) {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:@"UIDeviceOrientationDidChangeNotification" object:nil];

  }
  return self;
}

- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)deviceOrientationDidChange:(NSNotification *)notification
{
  UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
  
  NSString* orientationString = [self getOrientationStr:orientation];
  
  [self sendEventWithName:@"OrientationChanged" body:@{@"orientation": orientationString}];
  
}

- (NSString *)getOrientationStr: (UIDeviceOrientation)orientation {
  NSString *orientationStr;
  switch (orientation) {
    case UIDeviceOrientationPortrait:
      orientationStr = @"PORTRAIT";
      break;
    case UIDeviceOrientationLandscapeLeft:
    case UIDeviceOrientationLandscapeRight:
      
      orientationStr = @"LANDSCAPE";
      break;
      
    case UIDeviceOrientationPortraitUpsideDown:
      orientationStr = @"PORTRAITUPSIDEDOWN";
      break;
      
    default:
      orientationStr = @"UNKNOWN";
      break;
  }
  return orientationStr;
}

RCT_EXPORT_METHOD(lock:(NSNumber * _Nonnull)orientation) {
  [[UIDevice currentDevice] setValue:orientation forKey:@"orientation"];
}

RCT_EXPORT_METHOD(getOrientation:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject) {
  NSString *orientation = [self getCurrentOrientation];
  
  resolve(@{@"orienation": orientation});
}

- (NSString *) getCurrentOrientation {
  BOOL isLandscape = UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]);
  
  return isLandscape ? @"LANDSCAPE" : @"PORTRAIT";
}

- (NSDictionary *)constantsToExport
{
  return @{
           @"PORTRAIT"  : [NSNumber numberWithInt:UIInterfaceOrientationPortrait],
           @"LANDSCAPE" : [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft]
        };
}

@end
